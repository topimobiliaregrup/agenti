window.Vue = require('vue')
window.axios = require('axios')

let moment = require('vue-moment');
Vue.use(moment, {
    locale: 'ro'
});

import VueTippy from 'vue-tippy'
Vue.use(VueTippy)

import Notifications from 'vue-notification'
Vue.use(Notifications)

import VueCharts from 'vue-charts'
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

import VueTabs from 'vue-nav-tabs'
import VTab from 'vue-nav-tabs'
import 'vue-nav-tabs/themes/vue-tabs.css'
import tinymce from 'vue-tinymce-editor'
Vue.use(VueTabs)

Vue.use(VueCharts)
Vue.component('tinymce', tinymce)

import AnunturiRezidentiale from './components/anunturi/AnunturiRezidentiale.vue'
import AnunturiRezidentialeChirie from './components/anunturi/AnunturiRezidentialeChirie.vue'
import AnunturiComerciale from './components/anunturi/AnunturiComerciale.vue'
import AnunturiComercialeChirie from './components/anunturi/AnunturiComercialeChirie.vue'
import TotiFurnizorii from './components/clienti/TotiFurnizorii.vue'
import Clienti from './components/clienti/Clienti.vue'
import Rapoarte from './components/Rapoarte.vue'
import CautaRezidential from './components/content/CautaRezidential.vue'
import AdaugaSeo from './components/content/AdaugaSeo.vue'
import NotificariMesajeNoi from './components/NotificariMesajeNoi.vue'
import Chat from './components/Chat.vue'
import RichText from './components/RichText.vue'



new Vue({
	el: '#app',
	mounted () {
		//console.log('It Works')
	},
	components: {
		AnunturiRezidentiale,
		AnunturiRezidentialeChirie,
		AnunturiComerciale,
		AnunturiComercialeChirie,
		TotiFurnizorii,
		Clienti,
		Rapoarte,
		CautaRezidential,
		NotificariMesajeNoi,
		Chat,
		AdaugaSeo,
		RichText
	}
})