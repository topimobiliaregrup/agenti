@extends("agenti::layout.app")

@section('title')
	Baza clienților
@endsection

@section("page")

<!-- page content -->
<div class="right_col" role="main">
    <clienti user-Id="{{ Auth::User()->id }}"></clienti>
</div>
<!-- /page content -->
@endsection