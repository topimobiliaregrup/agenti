@extends("agenti::layout.app")

@section('title')
	Mesaje {{$source}}
@endsection

@section("page")
<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
    <div class="title_left">
      <h3>Mesaje (
				@php
					switch($source){
						case 'proiecte':
								echo "Proiecte noi";
								$citite = DB::connection('api')->table('contacts')->where('read', 1)->orderBy('id', 'DESC')->get();
								$necitite = DB::connection('api')->table('contacts')->where('read', 0)->orderBy('id', 'DESC')->get();
								break;

							case 'parteneri':
								echo "Imobile parteneri";
								$citite = DB::connection('parteneri')->table('messages')->where('read', 1)->orderBy('id', 'DESC')->get();
								$necitite = DB::connection('parteneri')->table('messages')->where('read', 0)->orderBy('id', 'DESC')->get();
								break;

							case 'cparteneri':
								echo "Contact parteneri";
								$citite = DB::connection('parteneri')->table('requests')->where('read', 1)->orderBy('id', 'DESC')->get();
								$necitite = DB::connection('parteneri')->table('requests')->where('read', 0)->orderBy('id', 'DESC')->get();
								break;

							default:
								echo "Proiecte noi";
								$citite = DB::connection('api')->table('contacts')->where('read', 1)->orderBy('id', 'DESC')->get();
								$necitite = DB::connection('api')->table('contacts')->where('read', 0)->orderBy('id', 'DESC')->get();
					}
				@endphp
      )</h3>
    </div>
  </div>
  <div class="clearfix"></div>
  <hr>
	<div class="row col-xs-12 mesaje">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Necitite</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Citite</a>
        </li>
      </ul>
      <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
          <div class="row">
          	<div class="col-xs-12">
          		<div class="x_content white-bg">
          			@if($source == "proiecte")
									<table class="table">
		          			<thead>
		          				<tr>
		          					<th>Data</th>
		          					<th>Nume</th>
		          					<th>Telefon</th>
		          					<th>Detalii</th>
		          					<th>Acțiuni</th>
		          				</tr>
		          			</thead>
		          			<tbody>
		          				@foreach($necitite as $i => $msg)
												<tr>
													<td>{{$msg->created_at}}</td>
													<td>{{$msg->nume}}</td>
													<td>{{$msg->telefon}}</td>
													<td>{{$msg->mesaj}}</td>
													<td>
														<a href="/new/vezi-mesaj/proiecte/{{$msg->id}}" class="btn btn-sm btn-info">
															<i class="fa fa-info-circle"></i>
														</a>
														&nbsp;
														<a href="/new/mesaj-citit/proiecte/{{$msg->id}}" class="btn btn-sm btn-success">
															<i class="fa fa-check"></i>
														</a>
													</td>
												</tr>
		          				@endforeach
		          			</tbody>
		          		</table>
          			@endif
          			{{-- End if Proiecte --}}

          			@if($source == "parteneri")
									<table class="table">
		          			<thead>
		          				<tr>
		          					<th>Data</th>
		          					<th>Nume</th>
		          					<th>Telefon</th>
		          					<th>Detalii</th>
		          					<th>Acțiuni</th>
		          				</tr>
		          			</thead>
		          			<tbody>
		          				@foreach($necitite as $i => $msg)
												<tr>
													<td>{{$msg->created_at}}</td>
													<td>{{$msg->nume}}</td>
													<td>{{$msg->telefon}}</td>
													<td>{{$msg->proprietate}}</td>
													<td>
														<a href="/new/vezi-mesaj/parteneri/{{$msg->id}}" class="btn btn-sm btn-info">
															<i class="fa fa-info-circle"></i>
														</a>
														&nbsp;
														<a href="/new/mesaj-citit/parteneri/{{$msg->id}}" class="btn btn-sm btn-success">
															<i class="fa fa-check"></i>
														</a>
													</td>
												</tr>
		          				@endforeach
		          			</tbody>
		          		</table>
          			@endif
          			{{-- End if Imobile Parteneri --}}

          			@if($source == "cparteneri")
									<table class="table">
		          			<thead>
		          				<tr>
		          					<th>Data</th>
		          					<th>Nume</th>
		          					<th>Telefon</th>
		          					<th>Detalii</th>
		          					<th>Acțiuni</th>
		          				</tr>
		          			</thead>
		          			<tbody>
		          				@foreach($necitite as $i => $msg)
												<tr>
													<td>{{$msg->created_at}}</td>
													<td>{{$msg->nume}}</td>
													<td>{{$msg->telefon}}</td>
													<td>{{$msg->detalii}}</td>
													<td>
														<a href="/new/vezi-mesaj/cparteneri/{{$msg->id}}" class="btn btn-sm btn-info">
															<i class="fa fa-info-circle"></i>
														</a>
														&nbsp;
														<a href="/new/mesaj-citit/cparteneri/{{$msg->id}}" class="btn btn-sm btn-success">
															<i class="fa fa-check"></i>
														</a>
													</td>
												</tr>
		          				@endforeach
		          			</tbody>
		          		</table>
          			@endif
          			{{-- End if Contact Parteneri --}}
	          		
          		</div>
          	</div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
          <div class="row">
          	<div class="col-xs-12">
          		<div class="x_content white-bg">
          			@if($source == "proiecte")
									<table class="table">
		          			<thead>
		          				<tr>
		          					<th>Data</th>
		          					<th>Nume</th>
		          					<th>Telefon</th>
		          					<th>Detalii</th>
		          					<th>Acțiuni</th>
		          				</tr>
		          			</thead>
		          			<tbody>
		          				@foreach($citite as $i => $msg)
												<tr>
													<td>{{$msg->created_at}}</td>
													<td>{{$msg->nume}}</td>
													<td>{{$msg->telefon}}</td>
													<td>{{$msg->mesaj}}</td>
													<td>
														<a href="/new/vezi-mesaj/proiecte/{{$msg->id}}" class="btn btn-sm btn-info">
															<i class="fa fa-info-circle"></i>
														</a>
													</td>
												</tr>
		          				@endforeach
		          			</tbody>
		          		</table>
          			@endif
          			{{-- End if Proiecte --}}

          			@if($source == "parteneri")
									<table class="table">
		          			<thead>
		          				<tr>
		          					<th>Data</th>
		          					<th>Nume</th>
		          					<th>Telefon</th>
		          					<th>Detalii</th>
		          					<th>Acțiuni</th>
		          				</tr>
		          			</thead>
		          			<tbody>
		          				@foreach($citite as $i => $msg)
												<tr>
													<td>{{$msg->created_at}}</td>
													<td>{{$msg->nume}}</td>
													<td>{{$msg->telefon}}</td>
													<td>{{$msg->proprietate}}</td>
													<td>
														<a href="/new/vezi-mesaj/parteneri/{{$msg->id}}" class="btn btn-sm btn-info">
															<i class="fa fa-info-circle"></i>
														</a>
													</td>
												</tr>
		          				@endforeach
		          			</tbody>
		          		</table>
          			@endif
          			{{-- End if Imobile Parteneri --}}

          			@if($source == "cparteneri")
									<table class="table">
		          			<thead>
		          				<tr>
		          					<th>Data</th>
		          					<th>Nume</th>
		          					<th>Telefon</th>
		          					<th>Detalii</th>
		          					<th>Acțiuni</th>
		          				</tr>
		          			</thead>
		          			<tbody>
		          				@foreach($citite as $i => $msg)
												<tr>
													<td>{{$msg->created_at}}</td>
													<td>{{$msg->nume}}</td>
													<td>{{$msg->telefon}}</td>
													<td>{{$msg->detalii}}</td>
													<td>
														<a href="/new/vezi-mesaj/cparteneri/{{$msg->id}}" class="btn btn-sm btn-info">
															<i class="fa fa-info-circle"></i>
														</a>
													</td>
												</tr>
		          				@endforeach
		          			</tbody>
		          		</table>
          			@endif
          			{{-- End if Contact Parteneri --}}
	          		
          		</div>
          	</div>
          </div>
        </div>
      </div>
    </div>
	</div>
    
</div>
<!-- /page content -->
@endsection