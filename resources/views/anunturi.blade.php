@extends("agenti::layout.app")

@section('title')
	Anunțuri rezidențiale
@endsection

@section('custom-js')

@endsection

@section("page")

<!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total anunțuri</span>
              <div class="count green">{{App\Ad::count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-home"></i> Apartamente</span>
              <div class="count">{{App\Ad::where('categoria', 'LIKE', '%Apartamente%')->count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-building"></i> Case</span>
              <div class="count">{{App\Ad::where('categoria', 'LIKE', '%Case%')->count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-map"></i> Ap. în blocuri noi</span>
              <div class="count">{{App\Ad::where('fond_locativ', 'LIKE', '%noi%')->count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-building-o"></i> Apartamente secundare</span>
              <div class="count">{{App\Ad::where('fond_locativ', 'LIKE', '%secundar%')->count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-briefcase"></i> La vânzare</span>
              <div class="count">{{App\Ad::where('tip', 'LIKE', '%Vînd%')->count()}}</div>
            </div>
          </div>
          <!-- /top tiles -->
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-sm-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><i class="fa fa-home"></i> Găsiți anunțuri rezidențiale</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Vânzare</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Chirie</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          <anunturi-rezidentiale :id="{{Auth::User()->id}}" />
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <anunturi-rezidentiale-chirie :id="{{Auth::User()->id}}" />
                        </div>
                      </div>
                    </div> 
                </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
@endsection