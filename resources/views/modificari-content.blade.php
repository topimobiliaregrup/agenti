@extends("agenti::layout.app")

@section('title')
	Însărcinări pentru content manager
@endsection

@section("page")

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Însărcinări pentru content manager</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group text-right">
            <button class="btn btn-info" id="adauga">Adaugă</button>
          </div>
        </div>
      </div>
    </div>

    <br><br><hr>
    
    <div class="row" id="modificare" style="display: none;">
    	<div class="col-md-8 col-md-offset-2 col-sm-12">
    		<div class="x_panel">
	        <div class="x_title">
	          <h2 style="display: block; width:100%;">Adaugă o însărcinare
	          	<button class="btn btn-sm btn-danger pull-right" id="inchide">
			           <i class="fa fa-times"></i> Închide
			         </button>
			      </h2>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

						<div class="row">
			        <form action="/modificari" method="POST">
			          {{csrf_field()}}
			          <input type="hidden" value="{{Auth::User()->id}}" name="user_id">
			          <div class="col-md-12">
			            <cauta-rezidential />
			          </div>
			        </form>
			      </div>
	          
	        </div>
	      </div>
    	</div>
    	<div class="col-xs-12">
    		<hr>
    	</div>
    </div>

 

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-bars"></i> Lista însărcinărilor</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">


        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Curente</a>
            </li>
            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Efectuate</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
              
							@php
					      $active = App\Change::where('status', 0)->get();
					    @endphp
					    <table class="table table-striped table-hover">
					      <tr>
					        <th>Adăugat</th>
					        <th>Proprietatea</th>
					        <th>Detalii</th>
					        <th>Actiuni</th>
					      </tr>
					      @foreach($active as $a)
					        <tr>
					          <td>{{$a->created_at}}</td>
					          <td>{{$a->proprietatea}}</td>
					          <td>{{$a->detalii}}</td>
					          <td>
					            <a href="/modificare/done/{{$a->id}}" class="btn btn-success">
					              <i class="fa fa-check"></i>
					            </a>
					          </td>
					        </tr>
					      @endforeach
					    </table>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
              	@php
				          $inactive = App\Change::where('status', 1)->orderBy('id','DESC')->get();
				        @endphp
				        <table class="table table-striped table-hover">
				          <tr>
				            <th>Adăugat</th>
				            <th>Executat</th>
				            <th>Proprietatea</th>
				            <th>Detalii</th>
				          </tr>
				          @foreach($inactive as $a)
				            <tr>
				              <td>{{$a->created_at}}</td>
				              <td>{{$a->updated_at}}</td>
				              <td>{{$a->proprietatea}}</td>
				              <td>{{$a->detalii}}</td>
				            </tr>
				          @endforeach
				        </table>
            </div>
          </div>
        </div>

      </div>
    </div>
</div>
<!-- /page content -->
@endsection

@section('custom-js')
<script>
	$(function () {
    $("#adauga").click(function () {
      $("#modificare").slideDown("500");
    });
    $("#inchide").click(function () {
      $("#modificare").slideUp("500");
    })
  })
</script>
@endsection