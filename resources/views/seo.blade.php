@extends("agenti::layout.app")

@section('title')
	SEO
@endsection


@section("page")

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Task-uri - SEO / Promovare</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group text-right">
            <button class="btn btn-info" id="adauga">Adaugă</button>
          </div>
        </div>
      </div>
    </div>

    <br><br><hr>
    
    <div class="row" id="modificare" style="display: none;">
    	<div class="col-md-8 col-md-offset-2 col-sm-12">
    		<div class="x_panel">
	        <div class="x_title">
	          <h2 style="display: block; width:100%;">Adaugă un task
	          	<button class="btn btn-sm btn-danger pull-right" id="inchide">
			           <i class="fa fa-times"></i> Închide
			         </button>
			      </h2>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

						<div class="row">
			        <form action="/new/add-task-seo" method="POST">
			          {{csrf_field()}}
			          <input type="hidden" value="{{Auth::User()->id}}" name="created_by">
			          <div class="col-md-12">
			            <adauga-seo />
			          </div>
			        </form>
			      </div>
	          
	        </div>
	      </div>
    	</div>
    	<div class="col-xs-12">
    		<hr>
    	</div>
    </div>

 

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-bars"></i> Lista task-urilor</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">


        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Curente</a>
            </li>
            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Efectuate</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
              
							@php
					      $active = App\Seo::where('status', 0)->get();
					    @endphp
					    <table class="table table-striped table-hover">
					      <tr>
					        <th>Adăugat</th>
					        <th>Titlu</th>
					        <th>Actiuni</th>
					      </tr>
					      @foreach($active as $a)
					      @php
					      	$usr = App\User::find($a->created_by);
					      	$dt = new Carbon\Carbon($a->created_at);
					      @endphp
					        <tr>
					          <td>
					          	<b>{{$dt->diffForHumans()}}</b> 
											<i>de către {{$usr->name}}</i>
					          </td>
					          <td>{{$a->titlu}}</td>
					          <td>
					            <a href="/new/seo-task/{{$a->id}}" class="btn btn-sm btn-info">
					              <i class="fa fa-info"></i> &nbsp; Vezi detalii
					            </a>
					          </td>
					        </tr>
					      @endforeach
					    </table>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
              	@php
						      $inactive = App\Seo::where('status', 1)->get();
						    @endphp
						    <table class="table table-striped table-hover">
						      <tr>
						        <th>Adăugat</th>
						        <th>Titlu</th>
						        <th>Actiuni</th>
						      </tr>
						      @foreach($inactive as $a)
						      @php
						      	$usr = App\User::find($a->created_by);
						      	$dt = new Carbon\Carbon($a->created_at);
						      @endphp
						        <tr>
						          <td>
					          	<b>{{$dt->diffForHumans()}}</b> 
											<i>de către {{$usr->name}}</i>
					          </td>
						          <td>{{$a->titlu}}</td>
						          <td>
						            <a href="/new/seo-task/{{$a->id}}" class="btn btn-sm btn-info">
						              <i class="fa fa-info"></i> &nbsp; Vezi detalii
						            </a>
						          </td>
						        </tr>
						      @endforeach
						    </table>
            </div>
          </div>
        </div>

      </div>
    </div>
</div>
<!-- /page content -->
@endsection

@section('custom-js')
<style>
@font-face {
  font-family: tinymce;
  src: url(https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.7/skins/lightgray/fonts/tinymce.woff);
  src: url(https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.7/skins/lightgray/fonts/tinymce.ttf);
}

</style>
<script>
	$(function () {
    $("#adauga").click(function () {
      $("#modificare").slideDown("500");
    });
    $("#inchide").click(function () {
      $("#modificare").slideUp("500");
    })
  })
</script>
@endsection