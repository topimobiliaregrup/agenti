@extends("agenti::layout.app")

@section('title')
	Însărcinări pentru agenți
@endsection

@section("page")

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Însărcinări pentru agenți</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group text-right">
            <button class="btn btn-info" id="adauga">Adaugă</button>
          </div>
        </div>
      </div>
    </div>

    <br><br><hr>
    
    <div class="row" id="modificare" style="display: none;">
    	<div class="col-md-8 col-md-offset-2 col-sm-12">
    		<div class="x_panel">
	        <div class="x_title">
	          <h2 style="display: block; width:100%;">Adaugă o însărcinare
	          	<button class="btn btn-sm btn-danger pull-right" id="inchide">
			           <i class="fa fa-times"></i> Închide
			         </button>
			      </h2>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

						<div class="row">
			        <form action="/modificari-agenti" method="POST">
			          {{csrf_field()}}
			          <input type="hidden" value="{{Auth::User()->id}}" name="user_id">
			          <div class="col-md-12">
			            <div class="form-group">
			              @php
			                $agenti = App\User::where('admin', '<>', 9)->get();
			              @endphp
			              Agentul: <br>
			              <select name="agent" class="form-control">
			                <option value="">Alegeți agentul</option>
			                <option value="toti">Toti agentii</option>
			                @foreach($agenti as $a)
			                  <option value="{{$a->id}}">{{$a->name}}</option>
			                @endforeach
			              </select>
			              
			            </div>
			            <div class="form-group">
			              Detalii: <br>
			              <textarea name="detalii" rows="5" class="form-control"></textarea>
			            </div>
			            <br>
		              <div class="form-group text-right">
	                  <button class="btn btn-success" type="submit">
	                    <i class="fa fa-save"></i> Adaugă
	                  </button>
		              </div>
			          </div>
			        </form>
			      </div>
	          
	        </div>
	      </div>
    	</div>
    	<div class="col-xs-12">
    		<hr>
    	</div>
    </div>

 

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-bars"></i> Lista însărcinărilor</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">


        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Curente</a>
            </li>
            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Efectuate</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
              
							@php
				        $active = App\ChangeAgent::where('status', 0)->get();
				      @endphp
				      <table class="table table-striped table-hover">
				        <tr>
				          <th>Adăugat</th>
				          <th>Agent</th>
				          <th>Detalii</th>
				          <th>Actiuni</th>
				        </tr>
				        @foreach($active as $a)
				        @php
				          $user = App\User::find($a->user_id);
				          $dt = new Carbon\Carbon($a->created_at);
				          $by = App\User::find($a->by);
				        @endphp
				          <tr>
				            <td><b>{{$dt->diffForhumans()}}</b> <i>de {{$by->name}}</i></td>
				            <td>{{$user->name}}</td>
				            <td>{{$a->detail}}</td>
				            <td>
				              <a href="/modificare-agenti/done/{{$a->id}}" class="btn btn-success">
				                <i class="fa fa-check"></i>
				              </a>
				            </td>
				          </tr>
				        @endforeach
				      </table>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
              	@php
				          $inactive = App\ChangeAgent::where('status', 1)->orderBy('id','DESC')->get();
				        @endphp
				      <table class="table table-striped table-hover">
				        <tr>
				          <th>Adăugat</th>
				          <th>Executat</th>
				          <th>Agent</th>
				          <th>Detalii</th>
				        </tr>
				        @foreach($inactive as $a)
				        @php
				          $user = App\User::find($a->user_id);
				          $dt = new Carbon\Carbon($a->created_at);
				          $dt1 = new Carbon\Carbon($a->updated_at);
				          $by = App\User::find($a->by);
				        @endphp
				          <tr>
				            <td><b>{{$dt->diffForHumans()}}</b> <i>de {{$by->name}}</i></td>
				            <td><b>{{$dt1->diffForHumans()}}</b></td>
				            <td>{{$user->name}}</td>
				            <td>{{$a->detail}}</td>
				          </tr>
				        @endforeach
				      </table>
            </div>
          </div>
        </div>

      </div>
    </div>
</div>
<!-- /page content -->
@endsection

@section('custom-js')
<script>
	$(function () {
    $("#adauga").click(function () {
      $("#modificare").slideDown("500");
    });
    $("#inchide").click(function () {
      $("#modificare").slideUp("500");
    })
  })
</script>
@endsection