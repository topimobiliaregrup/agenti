@extends("agenti::layout.app")

@section('title')
	Platforma agenților
@endsection

@section('custom-js')

@endsection

@section("page")

<!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Agenți</span>
              <div class="count">{{App\User::count()}}</div>
              {{-- <span class="count_bottom"><i class="green">4% </i> From last Week</span> --}}
              {{-- <a href="#"><span class="count_bottom"><i class="green fa fa-link"></i> Toți agenții</span></a> --}}
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-home"></i> Proprietăți rezidențiale</span>
              <div class="count">{{App\FurnizoriRezidential::count()}}</div>
              <a href="/new/toti-furnizorii"><span class="count_bottom"><i class="green fa fa-link"></i> Toți furnizorii</span></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-building"></i> Proprietăți comerciale</span>
              <div class="count">{{App\FurnizoriComercial::count()}}</div>
              <a href="/new/toti-furnizorii"><span class="count_bottom"><i class="green fa fa-link"></i> Toți furnizorii</span></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-map"></i> Terenuri</span>
              <div class="count">{{App\FurnizoriTeren::count()}}</div>
              <a href="/new/toti-furnizorii"><span class="count_bottom"><i class="green fa fa-link"></i> Toți furnizorii</span></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-building-o"></i> Proiecte rezidențiale noi</span>
              <div class="count">{{DB::connection('api')->table('projects')->count()}}</div>
              <a href="/new/proiecte"><span class="count_bottom"><i class="fa fa-link green"></i> Vezi proiectele</span></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-briefcase"></i> Companii de construcții</span>
              <div class="count">{{DB::connection('api')->table('companies')->count()}}</div>
              <a href="/new/proiecte/companii"><span class="count_bottom"><i class="fa green fa-link"></i> vezi companiile</span></a>
            </div>
          </div>
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-12">
                    <h3>Activitatea agenților <small>Adăugarea proprietăților în ultimile 7 zile</small></h3>
                  </div>
                </div>
								@php
								
								$now = \Carbon\Carbon::now();
								$arr = array();
								$k = 7;
									for($i = 0; $i < 7; $i++){
										$data[$i] = $now->subDay();
										$rezidential[$i] = App\FurnizoriRezidential::where('created_at', 'LIKE', $data[$i]->toDateString() . '%')->count();
										$comercial[$i] = App\FurnizoriComercial::where('created_at', 'LIKE', $data[$i]->toDateString() . '%')->count();
										$teren[$i] = App\FurnizoriTeren::where('created_at', 'LIKE', $data[$i]->toDateString() . '%')->count();
										array_push($arr, array($data[$i]->formatLocalized('%d %B'), $rezidential[$i], $comercial[$i], $teren[$i]));
										$k--;
									}
									$final = json_encode(array_reverse($arr));
								@endphp
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <vue-chart
					            chart-type="LineChart"
					            :columns="[{
									                  'type': 'string',
									                  'label': 'Year'
									              }, {
									                  'type': 'number',
									                  'label': 'Rezidential'
									              }, {
									                  'type': 'number',
									                  'label': 'Comercial'
									              }, {
									                  'type': 'number',
									                  'label': 'Terenuri'
									              }]"
					            :rows="{{ $final }}"
					            :options="{hAxis: {title: 'Data'},vAxis: {title: 'Număr de proprietăți'},height: 350, colors: ['#19bb9a', '#e94933', '#2f99e0']}"
					        ></vue-chart>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                  <div class="x_title">
                    <h2>Însărcinări efectuate</h2>
                    @php
                    	$contentDone = App\Change::where('status', 1)->count();
                    	$contentTotal = App\Change::count();

                    	$agentiDone = App\ChangeAgent::where('status', 1)->count();
                    	$agentiTotal = App\ChangeAgent::count();

                    	$content = number_format($contentDone * 100 / $contentTotal, 0, ".", "");
                    	$agenti = number_format($agentiDone * 100 / $agentiTotal, 0, ".", "");
                    	
                    @endphp
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>Content manager</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 100%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{$agenti}}"></div>
                        </div>
                        <p class="lead">Efectuate: <strong>{{$contentDone}}</strong> din <strong>{{$contentTotal}}</strong></p>
                      </div>
                    </div>
                    <div>
                      <p>Agenti</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 100%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{$agenti}}"></div>
                        </div>
                        <p class="lead">Efectuate: <strong>{{$agentiDone}}</strong> din <strong>{{$agentiTotal}}</strong></p>
                      </div>
                    </div>
                  </div>
                  {{-- <div class="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>Conventional Media</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>Bill boards</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                        </div>
                      </div>
                    </div>
                  </div> --}}

                </div>

                <div class="clearfix"></div>
              </div>
            </div>

          </div>
          <br />

          <div class="row">


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Ultimile mesaje „Proiecte noi”</h2>
                  @php
                  	$lastMessagesProjects = DB::connection('api')->table('contacts')->orderBy('id', 'DESC')->take(2)->get();
                  @endphp
                  <ul class="nav navbar-right panel_toolbox">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                    	@forelse ($lastMessagesProjects as $msg)
                    		<li>
	                        <div class="block">
	                          <div class="block_content">
	                            <h2 class="title">
	                              {{$msg->proprietate}}
	                            </h2>
	                            <div class="byline">
	                            	@php
	                            		$creat = new \Carbon\Carbon($msg->created_at);
	                            	@endphp
	                              <span>{{$creat->diffForHumans()}}</span> de <a>{{$msg->nume}}</a>
	                            </div>
	                            <p class="excerpt">
	                            	{{str_limit($msg->mesaj, 200, '...')}}
	                            </p>
	                          </div>
	                        </div>
	                      </li>
	                    @empty
	                    	<li>
	                        <div class="block">
	                          <div class="block_content">
	                            <h2 class="title">
	                              Nu sunt mesaje
	                            </h2>
	                          </div>
	                        </div>
	                      </li>
                    	@endforelse
                      
                    </ul>
                    <hr>
                    <a href="/new/mesaje/proiecte" class="btn btn-info pull-right">Vezi toate mesajele</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Ultimile mesaje „Parteneri”</h2>
                  @php
                  	$lastMessagesProjects = DB::connection('parteneri')->table('messages')->orderBy('id', 'DESC')->take(2)->get();
                  @endphp
                  <ul class="nav navbar-right panel_toolbox">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                    	@forelse ($lastMessagesProjects as $msg)
                    		<li>
	                        <div class="block">
	                          <div class="block_content">
	                            <h2 class="title">
	                              {{$msg->telefon}}
	                            </h2>
	                            <div class="byline">
	                            	@php
	                            		$creat = new \Carbon\Carbon($msg->created_at);
	                            	@endphp
	                              <span>{{$creat->diffForHumans()}}</span> de <a>{{$msg->nume}}</a>
	                            </div>
	                            <p class="excerpt">
	                            	{{ $msg->proprietate }}
	                            </p>
	                          </div>
	                        </div>
	                      </li>
	                    @empty
	                    	<li>
	                        <div class="block">
	                          <div class="block_content">
	                            <h2 class="title">
	                              Nu sunt mesaje
	                            </h2>
	                          </div>
	                        </div>
	                      </li>
                    	@endforelse
                      
                    </ul>
                    <hr>
                    <a href="/new/mesaje/parteneri" class="btn btn-info pull-right">Vezi toate mesajele</a>
                  </div>
                </div>
              </div>
            </div>

						<div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Ultimile cereri „Parteneri”</h2>
                  @php
                  	$lastMessagesProjects = DB::connection('parteneri')->table('requests')->orderBy('id', 'DESC')->take(2)->get();
                  @endphp
                  <ul class="nav navbar-right panel_toolbox">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                    	@forelse ($lastMessagesProjects as $msg)
                    		<li>
	                        <div class="block">
	                          <div class="block_content">
	                            <h2 class="title">
	                              {{$msg->categorie}}
	                            </h2>
	                            <div class="byline">
	                            	@php
	                            		$creat = new \Carbon\Carbon($msg->created_at);
	                            	@endphp
	                              <span>{{$creat->diffForHumans()}}</span> de <a>{{$msg->nume}}</a>
	                            </div>
	                            <p class="excerpt">
	                            	{{str_limit($msg->detalii, 200, '...')}}
	                            </p>
	                          </div>
	                        </div>
	                      </li>
	                    @empty
	                    	<li>
	                        <div class="block">
	                          <div class="block_content">
	                            <h2 class="title">
	                              Nu sunt mesaje
	                            </h2>
	                          </div>
	                        </div>
	                      </li>
                    	@endforelse
                      
                    </ul>
                    <hr>
                    <a href="/new/mesaje/cparteneri" class="btn btn-info pull-right">Vezi toate cererile</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <br>
          {{-- Start link-uri utile --}}
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Link-uri utile</h3>
                  </div>
                </div>

                
                <div class="col-md-4 col-sm-3 col-xs-12 bg-white">
                  <div class="x_title">
                    <h2>Generale</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <ul class="to_do">
                      <li>
                        <a href="https://www.cadastru.md/ecadastru/webinfo/f?p=100:1:1409543138435505" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> Cadastru
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="https://point.md/ro/map/47.02116691541881/28.847361803054806/18" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> Harta Moldovei
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="http://www.meteo2.md/ro/Prognoza_Meteo/Chisinau/Chisinau/" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> Prognoza meteo
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="http://bnm.md/ro/content/ratele-de-schimb" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> Curs Valutar BNM
                          </p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div class="col-md-4 col-sm-3 col-xs-12 bg-white">
                  <div class="x_title">
                    <h2>999.md</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <ul class="to_do">
                      <li>
                        <a href="https://999.md/ro/profile/TopImobiliare" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> TopImobiliare - Director
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="https://999.md/ro/profile/TopImobiliare-Alexandru" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> TopImobiliare-Alexandru - Alexandru Grosu
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="https://999.md/ro/profile/TopImobiliare-Chirii" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> TopImobiliare-Chirii - Chirii
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="https://999.md/ro/profile/topimobiliare-companie" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> TopImobiliare-companie - Fostul Rîșcani
                          </p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div class="col-md-4 col-sm-3 col-xs-12 bg-white">
                  <div class="x_title">
                    <h2>Top Imobiliare Grup</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <ul class="to_do">
                      <li>
                        <a href="https://topimobiliare.md/" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> Top Imobiliare Grup
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="https://proiecte.topimobiliare.md/" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> Proiecte rezidențiale noi
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="https://parteneri.topimobiliare.md/" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> Parteneri - Top Imobiliare Grup
                          </p>
                        </a>
                      </li>
                      <li>
                        <a href="https://cariera.topimobiliare.md/" target="_blank">
                          <p>
                            <i class="fa fa-link green"></i> Carieră
                          </p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div class="clearfix"></div>
              </div>
            </div>

          </div>

          {{-- End link-uri utile --}}
          <br>
          {{-- Start proiecte noi --}}
            <div class="row">
              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Proiecte rezidențiale noi</h2>
                    @php
                      $prs = DB::connection('api')->table('projects')->take(3)->inRandomOrder()->get();
                    @endphp
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    @foreach($prs as $pr)
                      <article class="h-55 media event">
                        <a class="pull-left date">
                          <img src="https://api.topimobiliare.md/storage/{{$pr->poza}}" alt="{{$pr->nume}}" class="img-responsive">
                        </a>
                        <div class="media-body">
                          <a class="title" href="https://proiecte.topimobiliare.md/proiecte/{{$pr->slug}}" target="_blank">{{$pr->nume}}</a>
                          <p>{!! str_limit($pr->meta_desc, 55, '...') !!}</p>
                        </div>
                      </article>
                    @endforeach
                    <hr>

                    <div class="media-footer text-right">
                        <a href="/new/proiecte" class="btn btn-info btn-sm">Vezi toate proiectele</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Companii de construcții</h2>
                    @php
                      $comps = DB::connection('api')->table('companies')->take(3)->inRandomOrder()->get();
                    @endphp
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    @foreach($comps as $c)
                      <article class="h-55 media event">
                        <a class="pull-left date">
                          <img src="https://api.topimobiliare.md/storage/{{$c->logo}}" alt="{{$c->nume}}" class="img-responsive">
                        </a>
                        <div class="media-body">
                          <a class="title" href="https://proiecte.topimobiliare.md/companii/{{$c->slug}}" target="_blank">{{$c->nume}}</a>
                          <p>{!! str_limit($c->meta_desc, 55, '...') !!}</p>
                        </div>
                      </article>
                    @endforeach
                    <hr>

                    <div class="media-footer text-right">
                        <a href="/new/proiecte/companii" class="btn btn-info btn-sm">Vezi toate companiile</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Chat <small>Ultimile mesaje</small></h2>
                    @php
                      $mesaje = App\Message::orderby('id', 'DESC')->take(3)->get();
                    @endphp
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    @foreach($mesaje as $msg)
                    @php
                      $usr = App\User::find($msg->user_id);
                      $dt = new Carbon\Carbon($msg->created_at);
                    @endphp
                    <article class="h-55 media event">
                      <a class="pull-left date">
                        @if($usr->avatar !== null)
                          <img src="{{$usr->avatar}}" alt="" class="img-responsive">
                        @else
                          <img src="/assets/no-image.jpg" alt="" class="img-responsive">
                        @endif
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">{{$usr->name}}</a> <small class="pull-right">{{$dt->diffForHumans()}}</small>
                        <p>{{str_limit($msg->message, 55, '...')}}</p>
                      </div>
                    </article>
                    @endforeach
                    <hr>

                    <div class="media-footer text-right">
                        <a href="/new/chat" class="btn btn-info btn-sm">Chat</a>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          {{-- End proiecte noi --}}

        </div>
        <!-- /page content -->
@endsection