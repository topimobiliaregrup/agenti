@extends("agenti::layout.app")

@section('title')
	Proiecte rezidențiale noi
@endsection

@section("page")

<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
    <div class="title_left">
      <h3>Proiecte rezidențiale noi</h3>
    </div>
  </div>
  <div class="clearfix"></div>
  <hr>
  <div class="row">
  	<div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Toate proiectele noi</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        	@php
        		$projects = DB::connection('api')->table('projects')->get();
        	@endphp
          <table style="font-size: 15px;" id="datatable1" class="table table-striped table-bordered dataTable no-footer">
            <thead>
              <tr>
              	<th>Nume</th>
              	<th>Compania</th>
              	<th>Adresa</th>
              	<th>Exploatare</th>
              	<th>Prețul</th>
              	<th>Acțiuni</th>
              </tr>
            </thead>
						<tbody>
            	@foreach($projects as $pr)
            	@php
            		$compania = DB::connection('api')->table('companies')->where('id', $pr->companie)->first();
            	@endphp
	            	<tr>
	            		<td>{{$pr->nume}}</td>
	            		<td>
	            			<a href="https://proiecte.topimobiliare.md/companii/{{$compania->slug}}" target="_blank" rel="noopener">
	            				{{$compania->nume}}
	            			</a>
	            		</td>
	            		<td>{{$pr->oras}}, {{$pr->sector}}, {{$pr->adresa}}</td>
	            		<td>{{$pr->exploatare}}</td>
	            		<td>de la {{$pr->pret}} €</td>
	            		<td>
	            			<a href="https://proiecte.topimobiliare.md/proiecte/{{$pr->slug}}" class="btn btn-xs btn-danger" target="_blank">
	            				Vezi proiectul
	            			</a>
	            		</td>
	              </tr>
              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
@endsection

@section('custom-js')
	<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script>
		$(function () {
			$("#datatable1").DataTable({
				"language": {
            "lengthMenu": "Arată  _MENU_  proiecte pe o pagină",
            "zeroRecords": "Ne pare rău, n-am găsit nimic după cererea dvs.",
            "info": "Pagina _PAGE_ din _PAGES_",
            "infoEmpty": "Nu sunt proiecte",
            "infoFiltered": "(Filtrat din _MAX_ proiecte)"
        },
        "pageLength": 25
			});
		})
	</script>
@endsection