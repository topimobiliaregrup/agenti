@extends("agenti::layout.app")

@section('title')
	Chat
@endsection

@section("page")

<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
    <div class="title_left">
      <h3>Chat intern</h3>
    </div>
  </div>
  <div class="clearfix"></div>
  <hr>
  <chat myid="{{Auth::User()->id}}" />
</div>
<!-- /page content -->
@endsection