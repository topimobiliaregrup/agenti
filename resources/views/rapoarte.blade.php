@extends("agenti::layout.app")

@section('title')
	Rapoarte
@endsection

@section("page")

<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
    <div class="title_left">
      <h3>Rapoarte</h3>
    </div>
  </div>
    <rapoarte></rapoarte>
</div>
<!-- /page content -->
@endsection