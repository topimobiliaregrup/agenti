<html lang="ro"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Platforma agenților imobiliari - Top Imobiliare Grup</title>
		<link rel="icon" type="image/png" href="https://proiecte.topimobiliare.md/apple-icon-57x57.png" />
    <!-- Bootstrap -->
    <link href="/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="/assets/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/assets/custom.min.css" rel="stylesheet">
    <style>
    	.mt-100{
    		margin-top: 100px;
    	}
    </style>
  </head>

  <body class="login" style="overflow:  hidden;">
    <div>
			<div class="container">
				<div class="row mt-100">
					{{-- <div class="col-md-6 col-md-offset-3">
						<h3 class="text-center">Salut, stimați și muncitori* colegi.</h3>
						<h2 class="text-center">Eu v-am pregătit o suprpiză. Pentru comoditatea voastră și pentru a vă ușura lucrul, eu am modificat puțin platforma pentru agenți. Adică cum puțin? Destul de mult. Sper să vă placă</h2>
						<br>
						<h6 class="text-right">Sincerely, admin-ul vostru <a href="https://facebook.com/MrColitza13">Nicolae Casîr</a> =)</h6>
					</div> --}}
				</div>
			</div>
		
      <div class="login_wrapper" style="margin:0px auto 0;">
        <div class="animate form login_form">
          <section class="login_content">
          	<br><br>
            <form method="post" action="/login">
            	{{csrf_field()}}
              <h1>Logare</h1>
              <div>
                <input type="text" class="form-control" placeholder="Email" required="" name="email">
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Parola" required="" name="password">
              </div>
              <div>
              	<label>
              		<input type="checkbox" name="remember" checked> Ține-mă minte
                </label>
              </div>
              <div>
                <button class="btn btn-default submit" type="submit">Logare</button>
                
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br>

                <div>
                  <h1>Platforma agenților imobiliari</h1>
                  <p>© 2018 <a href="https://topimobliare.md/" target="_blank">Top Imobiliare Grup</a>. Toate drepturile rezervate.</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  <small style="position: absolute; margin: 0 auto; bottom: 5px; right: 50px;">
  	<strong>*</strong> - cu excepția lui Andrușa.
  </small>

</body></html>