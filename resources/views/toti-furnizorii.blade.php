@extends("agenti::layout.app")

@section('title')
	Toți furnizorii
@endsection

@section("page")

<!-- page content -->
<div class="right_col" role="main">
    <toti-furnizorii :user_id="{{Auth::User()->id}}" :nume_agent="'{{Auth::User()->name}}'" />
</div>
<!-- /page content -->
@endsection