@extends("agenti::layout.app")

@section('title')
	Anunțuri comerciale
@endsection

@section('custom-js')

@endsection

@section("page")

<!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total anunțuri</span>
              <div class="count green">{{App\Adc::count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-briefcase"></i> Oficii</span>
              <div class="count">{{App\Adc::where('categorie', 'LIKE', '%Oficii%')->count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-building"></i> Încăperi comerciale</span>
              <div class="count">{{App\Adc::where('categorie', 'LIKE', '%comerciale%')->count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-beer"></i> Cafenele, Baruri</span>
              <div class="count">{{App\Adc::where('categorie', 'LIKE', '%Cafenele%')->count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Vânzare</span>
              <div class="count">{{App\Adc::where('tip', 'LIKE', '%Vînd%')->count()}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-retweet"></i> Chirie</span>
              <div class="count">{{App\Adc::where('tip', 'LIKE', '%Chirie%')->count()}}</div>
            </div>
          </div>
          <!-- /top tiles -->
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-sm-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><i class="fa fa-building-o"></i> Găsiți anunțuri comerciale</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Vânzare</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Chirie</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          <anunturi-comerciale :userid="{{Auth::User()->id}}" />
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <anunturi-comerciale-chirie :userid="{{Auth::User()->id}}" />
                        </div>
                      </div>
                    </div> 
                </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
@endsection