@if(Auth::guest())
  @php
    header('Location: /new/login'); 
    die();
  @endphp
@endif
<!DOCTYPE html>
<html lang="ro">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <link rel="icon" type="image/png" href="https://proiecte.topimobiliare.md/apple-icon-57x57.png" />

    <title>
			@yield('title') - Top Imobiliare Grup
    </title>

    <!-- Bootstrap -->
    <link href="/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    {{-- <link href="/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
	
    <!-- bootstrap-progressbar -->
    <link href="/assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    {{-- <link href="/assets/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/> --}}
    <!-- bootstrap-daterangepicker -->
    {{-- <link href="/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"> --}}

    <!-- Custom Theme Style -->
    <link href="/assets/custom.min.css" rel="stylesheet">
    <link href="/assets/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>
@php
  $user = Auth::User();
  $proiecte = DB::connection('api')->table('contacts')->where('read', 0)->get();
  $parteneri = DB::connection('parteneri')->table('messages')->where('read', 0)->get();
  $p_client = DB::connection('parteneri')->table('requests')->where('read', 0)->get();
  $total_msg = $proiecte->count() + $parteneri->count() + $p_client->count();
@endphp
  <body class="nav-md">
    <div class="container body" id="app">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/new/" class="site_title"><span>Platforma agenților</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img 
	                @if($user->avatar != "") 
	                	src="{{$user->avatar}}" 
	                @else 
	                	src="/assets/no-image.jpg" 
	                @endif 
	               alt="{{$user->name}}" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Salut,</span>
                <h2>{{$user->name}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br>
            @php
              $seo = App\Seo::where('status', 0)->count();
              $ag  = App\ChangeAgent::where('status', 0)->count();
              $cont  = App\Change::where('status', 0)->count();
            @endphp
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li> <a href="/new/"><i class="fa fa-home"></i> Pagina principală</a> </li>
                  <li><a><i class="fa fa-list-alt"></i> Găsiți anunțuri <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/new/anunturi">Rezidențiale</a></li>
                      <li><a href="/new/anunturic">Comerciale</a></li>
                    </ul>
                  </li>
                  <li><a href="/new/rapoarte"><i class="fa fa-flag-checkered"></i> Rapoarte</a></li>

                  <li><a><i class="fa fa-users"></i> Baza clienților <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/new/baza-clienti">Clienți</a></li>
                      <li><a href="/new/toti-furnizorii">Furnizori</a></li>
                    </ul>
                  </li>
                  @if(Auth::User()->admin == 5)
                    <li><a href="/new/seo"><i class="fa fa-search"></i> SEO / Promovare
                      @if($seo > 0)
                        &nbsp; <span class="label label-danger bold"> {{$seo}} </span>
                      @endif
                    </a></li>
                  @endif
                  <li><a><i class="fa fa-book"></i> Însărcinări 
                    @if($seo + $ag + $cont > 0)
                      &nbsp; <span class="label label-danger bold"> {{$seo + $ag + $cont}} </span>
                    @endif
                    <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/new/modificari-agenti">Pentru agenți
                        @if($ag > 0)
                          &nbsp; <span class="label label-danger bold"> {{$ag}} </span>
                        @endif
                      </a></li>
                      <li><a href="/new/modificari-content">Pentru content manager
                        @if($cont > 0)
                          &nbsp; <span class="label label-danger bold"> {{$cont}} </span>
                        @endif
                      </a></li>
                      @if(Auth::User()->admin != 5)
                        <li><a href="/new/seo">SEO / Promovare
                          @if($seo > 0)
                            &nbsp; <span class="label label-danger bold"> {{$seo}} </span>
                          @endif
                        </a></li>
                      @endif
                    </ul>
                  </li>
                  <li><a><i class="fa fa-envelope"></i> Mesaje 
                        @if($total_msg > 0) 
                          <span class="label label-danger bold">{{$total_msg}} noi</span>
                        @endif
                        <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/new/mesaje/proiecte">Proiecte noi
                        @if($proiecte->count() > 0) 
                          <span class="label label-danger bold">{{$proiecte->count()}} noi</span>
                        @endif
                      </a></li>
                      <li><a href="/new/mesaje/parteneri">Imobile parteneri
                        @if($parteneri->count() > 0) 
                          <span class="label label-danger bold">{{$parteneri->count()}} noi</span>
                        @endif
                      </a></li>
                      <li><a href="/new/mesaje/cparteneri">Contact parteneri
                        @if($p_client->count() > 0) 
                          <span class="label label-danger bold">{{$p_client->count()}} noi</span>
                        @endif
                      </a></li>
                    </ul>
                  </li>
                  <li><a href="/new/chat"><i class="fa fa-comments"></i> Chat</a></li>
                  <li><a><i class="fa fa-building"></i> Proiecte noi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/new/proiecte/companii">Companii de construcții</a></li>
                      <li><a href="/new/proiecte">Proiecte</a></li>
                    </ul>
                  </li>
                  {{-- <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="general_elements.html">General Elements</a></li>
                      <li><a href="media_gallery.html">Media Gallery</a></li>
                      <li><a href="typography.html">Typography</a></li>
                      <li><a href="icons.html">Icons</a></li>
                      <li><a href="glyphicons.html">Glyphicons</a></li>
                      <li><a href="widgets.html">Widgets</a></li>
                      <li><a href="invoice.html">Invoice</a></li>
                      <li><a href="inbox.html">Inbox</a></li>
                      <li><a href="calendar.html">Calendar</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="tables.html">Tables</a></li>
                      <li><a href="tables_dynamic.html">Table Dynamic</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="chartjs.html">Chart JS</a></li>
                      <li><a href="chartjs2.html">Chart JS2</a></li>
                      <li><a href="morisjs.html">Moris JS</a></li>
                      <li><a href="echarts.html">ECharts</a></li>
                      <li><a href="other_charts.html">Other Charts</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
                      <li><a href="fixed_footer.html">Fixed Footer</a></li>
                    </ul>
                  </li> --}}
                </ul>
              </div>
              {{-- <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a></li>
                      <li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="page_403.html">403 Error</a></li>
                      <li><a href="page_404.html">404 Error</a></li>
                      <li><a href="page_500.html">500 Error</a></li>
                      <li><a href="plain_page.html">Plain Page</a></li>
                      <li><a href="login.html">Login Page</a></li>
                      <li><a href="pricing_tables.html">Pricing Tables</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>                  
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                </ul>
              </div> --}}

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            {{-- <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> --}}
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img 
			                @if($user->avatar != "") 
			                	src="{{$user->avatar}}" 
			                @else 
			                	src="/assets/no-image.jpg" 
			                @endif 
			               alt="{{$user->name}}">{{$user->name}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    {{-- <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li> --}}
                    <li><a href="/new/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                
                  {{-- Notificari --}}
                    <notificari-mesaje-noi />
                  {{-- End notificari --}}
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        @yield('page')

        <!-- footer content -->
        <footer>
          <div class="pull-left">
            © 2018 - <a href="https://topimobiliare.md">Top Imobiliare Grup</a>
          </div>
          <div class="pull-right">
            Made with <i class="fa fa-heart-o text-danger"></i> by <a href="https://facebook.com/MrColitza13">Nicolae Casîr</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

		{{-- Vue <3 --}}
		<script src="/assets/app.js"></script>
    
    <!-- jQuery -->
    <script src="/assets/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    {{-- <script src="/assets/vendors/Chart.js/dist/Chart.min.js"></script> --}}
    <!-- gauge.js -->
    {{-- <script src="/assets/vendors/gauge.js/dist/gauge.min.js"></script> --}}
    <!-- bootstrap-progressbar -->
    <script src="/assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    {{-- <script src="/assets/vendors/iCheck/icheck.min.js"></script> --}}
    <!-- Skycons -->
    {{-- <script src="/assets/vendors/skycons/skycons.js"></script> --}}
    <!-- Flot -->
    {{-- <script src="/assets/vendors/Flot/jquery.flot.js"></script>
    <script src="/assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/assets/vendors/Flot/jquery.flot.resize.js"></script> --}}
    <!-- Flot plugins -->
    {{-- <script src="/assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/assets/vendors/flot.curvedlines/curvedLines.js"></script> --}}
    <!-- DateJS -->
    <script src="/assets/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    {{-- <script src="/assets/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script> --}}
    <!-- bootstrap-daterangepicker -->
    {{-- <script src="/assets/vendors/moment/min/moment.min.js"></script>
    <script src="/assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script> --}}

    <!-- Custom Theme Scripts -->
    <script src="/assets/custom.min.js"></script>
	 @yield("custom-js")
  </body>
</html>
