@extends("agenti::layout.app")

@section('title')
Companii de construcții
@endsection

@section("page")

<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
    <div class="title_left">
      <h3>Companii de construcții</h3>
    </div>
  </div>
  <div class="clearfix"></div>
  <hr>
  @php
  		$companii = DB::connection('api')->table('companies')->get();
  	@endphp
  <div class="row">
  	@foreach($companii as $c)
  	<div class="col-md-4 col-sm-4 col-xs-12 profile_details">
      <div class="well profile_view">
        <div class="col-sm-12">
          <h4 class="brief"><i>{{$c->nume}}</i></h4>
          <div class="left col-xs-7">
            <h2><i class="fa fa-user"></i> {{ $c->contact_name}}</h2>
            <br>
            <ul class="list-unstyled" style="font-size: 14px;">
              <li><i class="fa fa-building"></i> Adresa: {{$c->adresa}}</li>
              <br>
              <li><i class="fa fa-phone"></i> Telefon: {{$c->telefon}}</li>
            </ul>
          </div>
          <div class="right col-xs-5 text-center">
            <img src="https://api.topimobiliare.md/storage/{{$c->logo}}" alt="" class="img-responsive">
          </div>
        </div>
        <div class="col-xs-12 bottom text-center">
          <div class="col-xs-12 col-sm-6 emphasis">
            <p class="ratings">
              <a href="{{$c->website}}" target="_blank" rel="nofollow noopener"><span class="fa fa-globe"></span> Website</a>
            </p>
          </div>
          <div class="col-xs-12 col-sm-6 emphasis text-right">
            <a target="_blank" href="https://proiecte.topimobiliare.md/companii/{{$c->slug}}" class="btn btn-success btn-xs"> <i class="fa fa-link">
              </i> Pagina companiei </a>
            </div>
        </div>
      </div>
    </div>
    @endforeach

  </div>
</div>
<!-- /page content -->
@endsection
