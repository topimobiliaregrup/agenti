@extends("agenti::layout.app")

@section('title')
	Vezi mesajul
@endsection

@section("page")
<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
    <div class="title_left">
      <h3>Mesaj de la {{$mesaj->nume}}</h3>
    </div>
  </div>
  <div class="clearfix"></div>
  <hr>
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3">
			{{-- Daca e de la proiecte noi --}}
			@if($source == "proiecte")
				<div class="x_panel">
          <div class="x_title">
            <h2 style="width:100%"><i class="fa fa-envelope"></i> Mesaj - proiecte noi
            	<a href="/new/mesaje/proiecte" class="pull-right btn btn-info btn-xs">
                        </i> <i class="fa fa-arrow-left"></i> Înapoi la toate mesajele </a>
             </h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
						<div class="row">
							<div class="col-xs-12 profile_details">
                <div class="well profile_view">
                  <div class="col-sm-12">
                  	@php
                  		$dt = new \Carbon\Carbon($mesaj->created_at);
                  	@endphp
                    <h4 class="brief"><i>{{$mesaj->created_at}} ({{ $dt->diffForHumans() }})</i></h4>
                    <div class="left col-xs-12">
                      <h2><strong>Nume: </strong> {{$mesaj->nume}}</h2>
                      <br>
                      <p class="lead"><strong>Mesaj: </strong> {{$mesaj->mesaj}} </p>
                      <br>
                      <ul class="list-unstyled">
                        <li style="font-size:16px;"><i class="fa fa-phone"></i> Telefon: {{$mesaj->telefon}}</li>
                      </ul>
                    </div>
                    {{-- <div class="right col-xs-5 text-center">
                      <img src="images/img.jpg" alt="" class="img-circle img-responsive">
                    </div> --}}
                  </div>
                  <div class="col-xs-12 bottom text-center">
                    {{-- <div class="col-xs-12 col-sm-6 emphasis">
                      <p class="ratings">
                        <a>4.0</a>
                        <a href="#"><span class="fa fa-star"></span></a>
                        <a href="#"><span class="fa fa-star"></span></a>
                        <a href="#"><span class="fa fa-star"></span></a>
                        <a href="#"><span class="fa fa-star"></span></a>
                        <a href="#"><span class="fa fa-star-o"></span></a>
                      </p>
                    </div> --}}
                    <div class="col-xs-12 emphasis">
                    	@if($mesaj->read == 0)
                      <a href="/new/mesaj-citit/proiecte/{{$mesaj->id}}" class="pull-left btn btn-success btn-xs">
                        </i> <i class="fa fa-check"></i> Marchează ca citit </a>
                      @endif
                      <a target="_blank" rel="noopener" href="https://proiecte.topimobiliare.md{{$mesaj->de_unde}}" class="pull-right btn btn-danger btn-xs">
                        <i class="fa fa-link"> </i> Vezi proprietatea
                      </a>
                    </div>
                  </div>
                </div>
              </div>
						</div>
          </div>
        </div>
			@endif
			{{-- End daca e de la proiecte --}}

			{{-- Daca e de la imobile parteneri --}}
			@if($source == "parteneri")
      	@php
      		$link = explode('/', $mesaj->proprietate);
      		if(str_contains($link[3], "ofic") || str_contains($link[3], "comercial")){
      			$prop = DB::connection('parteneri')->table('comercials')->where('id', $mesaj->id_proprietate)->first();
      		} else if(str_contains($link[3], "apartament") || str_contains($link[3], "case")){
      			$prop = DB::connection('parteneri')->table('rezidentials')->where('id', $mesaj->id_proprietate)->first();
      		}
      		else if(str_contains($link[3], "teren")){
      			$prop = DB::connection('parteneri')->table('terens')->where('id', $mesaj->id_proprietate)->first();
      		} else {
      			redirect()->intended('/new');
      		}
      		$dt = new \Carbon\Carbon($mesaj->created_at);
      	@endphp
				<div class="x_panel">
          <div class="x_title">
            <h2 style="width:100%"><i class="fa fa-envelope"></i> Mesaj - {{$prop->numele}}
            	<a href="/new/mesaje/parteneri" class="pull-right btn btn-info btn-xs">
                        </i> <i class="fa fa-arrow-left"></i> Înapoi la toate mesajele </a>
             </h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
						<div class="row">
							<div class="col-xs-12 profile_details">
                <div class="well profile_view">
                  <div class="col-sm-12">
                    <h4 class="brief"><i>{{$mesaj->created_at}} ({{ $dt->diffForHumans() }})</i></h4>
                    <div class="left col-xs-7">
                      <p class="lead"><strong><i class="fa fa-user"></i> Nume: </strong> {{$mesaj->nume}} </p>
                      <br>
                      <p class="lead"><strong><i class="fa fa-phone"></i> Telefon: </strong> {{$mesaj->telefon}} </p>
                      <br>
                      <p class="lead"><strong>Preț proprietate:</strong> {{$prop->pret}} €</p>
                    </div>
                    <div class="right col-xs-5 text-center">
                      <img src="{{$prop->poza}}" alt="Imaginea proprietatii" class="img-responsive" style="border:2px solid;">
                    </div>
                  </div>
                  <div class="col-xs-12 bottom text-center">
                    <div class="col-xs-12 emphasis">
                    	@if($mesaj->read == 0)
                      <a href="/new/mesaj-citit/parteneri/{{$mesaj->id}}" class="pull-left btn btn-success btn-xs">
                        </i> <i class="fa fa-check"></i> Marchează ca citit </a>
                      @endif
                      <a target="_blank" rel="noopener" href="{{$mesaj->proprietate}}" class="pull-right btn btn-danger btn-xs">
                        <i class="fa fa-link"> </i> Vezi proprietatea
                      </a>
                    </div>
                  </div>
                </div>
								
								<p class="lead">Detalii</p>
								<table class="table">
									<tr>
										<td>Companie:</td>
										<td>{{$prop->companie}}</td>
									</tr>
									<tr>
										<td>Nume partener:</td>
										<td>{{$prop->nume_partener}}</td>
									</tr>
									<tr>
										<td>Telefon:</td>
										<td>{{$prop->telefon}}</td>
									</tr>
								</table>

              </div>
						</div>
          </div>
        </div>
			@endif
			{{-- End daca e de la imobile parteneri --}}


			{{-- Daca e de la contact parteneri--}}
			@if($source == "cparteneri")
				<div class="x_panel">
          <div class="x_title">
            <h2 style="width:100%"><i class="fa fa-envelope"></i> Mesaj - Contact parteneri
            	<a href="/new/mesaje/cparteneri" class="pull-right btn btn-info btn-xs">
                        </i> <i class="fa fa-arrow-left"></i> Înapoi la toate mesajele </a>
             </h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
						<div class="row">
							<div class="col-xs-12 profile_details">
                <div class="well profile_view">
                  <div class="col-sm-12">
                  	@php
                  		$dt = new \Carbon\Carbon($mesaj->created_at);
                  	@endphp
                    <h4 class="brief"><i>{{$mesaj->created_at}} ({{ $dt->diffForHumans() }})</i></h4>
                    <div class="left col-xs-12">
                      <h2><strong>Nume: </strong> {{$mesaj->nume}}</h2>
                      <h2><strong>Caută: </strong> {{$mesaj->categorie}}</h2>
                      <br>
                      <p class="lead"><strong>detalii: </strong> {{$mesaj->detalii}} </p>
                      <br>
                      <ul class="list-unstyled">
                        <li style="font-size:16px;"><i class="fa fa-phone"></i> Telefon: {{$mesaj->telefon}}</li>
                      </ul>
                    </div>
                    {{-- <div class="right col-xs-5 text-center">
                      <img src="images/img.jpg" alt="" class="img-circle img-responsive">
                    </div> --}}
                  </div>
                  <div class="col-xs-12 bottom text-center">
                    
                    <div class="col-xs-12 emphasis">
                    	@if($mesaj->read == 0)
                      <a href="/new/mesaj-citit/cparteneri/{{$mesaj->id}}" class="pull-left btn btn-success btn-xs">
                        </i> <i class="fa fa-check"></i> Marchează ca citit </a>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
						</div>
          </div>
        </div>
			@endif
			{{-- End daca e de la contact parteneri --}}
		</div>
	</div>
    
</div>
<!-- /page content -->
@endsection