@extends("agenti::layout.app")

@section('title')
	Vezi task-ul SEO
@endsection


@section("page")

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Task-uri - SEO / Promovare</h3>
        @php
        	$user = App\User::find($task->created_by);
        @endphp
      </div>

    </div>
    <div class="clearfix"></div>
    <hr>

 	<div class="row">
 		<div class="col-sm-12 col-md-6">
 			<div class="x_panel">
	      <div class="x_title">
	        <h2><i class="fa fa-angle-right"></i> {{$task->titlu}} <small>de către {{$user->name}}</small></h2>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">

					<div class="row">
						<div class="col-xs-12">
							{!! $task->content !!}
						</div>
					</div>

	      </div>
	    </div>
 		</div>
 		<div class="col-sm-12 col-md-6">
	 		<div class="x_panel">
	      <div class="x_title">
	        <h2><i class="fa fa-check"></i> 
	        @if($task->status == 1)
	        	Task efectuat
	        @else
	      		Marchează ca efectuat
	      	@endif</h2>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
					@if($task->status == 1)
						<div class="row">
							<div class="col-xs-12">
								<p class="lead">Detalii:</p>
								<div class="detalii">
									{!! $task->comment !!}
								</div>
							</div>
						</div>
					@else
						<div class="row">
							<div class="col-xs-12">
								<form action="/new/done-seo-task" method="POST">
									{{csrf_field()}}
									<input type="hidden" value="{{$task->id}}" name="id">
									<p class="lead">
										Vă rugăm să scrieți mai jos detalii privind acest task, dacă există, desigur.
									</p>
									<br>
									<div>
										<rich-text :nume="'comment'" />
									</div>
									<br>
								  <div class="col-md-12 text-right">
								    <div class="form-group">
								        <button class="btn btn-primary" type="submit">
								          <i class="fa fa-save"></i> Marchează ca efectuat
								        </button>
								    </div>
								  </div>
								</form>
							</div>
						</div>
					@endif

	      </div>
	    </div>
	  </div>
 	</div>

</div>
<!-- /page content -->
@endsection

@section('custom-js')
<style>
@font-face {
  font-family: tinymce;
  src: url(https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.7/skins/lightgray/fonts/tinymce.woff);
  src: url(https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.7/skins/lightgray/fonts/tinymce.ttf);
}

</style>
<script>
	$(function () {
    $("#adauga").click(function () {
      $("#modificare").slideDown("500");
    });
    $("#inchide").click(function () {
      $("#modificare").slideUp("500");
    })
  })
</script>
@endsection