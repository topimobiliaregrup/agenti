@extends("agenti::layout.app")

@section('title')
	SEO / Promovare
@endsection


@section("page")

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>SEO / Promovare</h3>
      </div>
    </div>
	<div class="clearfix"></div>
	<hr>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Salut, {{Auth::User()->name}}</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" style="width: 80%" src="{{Auth::User()->avatar}}" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>{{Auth::User()->name}}</h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-envelope-square user-profile-icon"></i> <a href="mailto:{{$user->gmail}}">{{$user->gmail}}</a>
                        </li>

                        <li>
                          <i class="fa fa-facebook-square user-profile-icon"></i> <a href="{{$user->facebook}}" target="_blank">Facebook</a>
                        </li>
												
												@if($user->skype !== null)
													<li>
	                          <i class="fa fa-skype user-profile-icon"></i> {{$user->skype}}
	                        </li>
												@endif
												@if($user->telefon !== null)
													<li>
	                          <i class="fa fa-mobile user-profile-icon"></i> {{$user->telefon}}
	                        </li>
												@endif
                      </ul>

                      {{-- <a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Editează Profilul</a> --}}
                      <br>

                      <!-- start links -->
                      <div class="x_title">
		                    <h2>Link-uri utile</h2>
		                    <div class="clearfix"></div>
		                  </div>
		                  <div class="x_content">
		                    <ul class="to_do">
		                      <li>
		                        <a href="https://facebook.com/topimobiliaregrup" target="_blank">
		                          <p>
		                            <i class="fa fa-link green"></i> Facebook Top Imobiliare
		                          </p>
		                        </a>
		                      </li>
		                      <li>
		                        <a href="https://analytics.google.com/analytics/web/" target="_blank">
		                          <p>
		                            <i class="fa fa-link green"></i> Google Analytics
		                          </p>
		                        </a>
		                      </li>
		                      <li>
		                        <a href="https://www.google.com/webmasters/tools/home" target="_blank">
		                          <p>
		                            <i class="fa fa-link green"></i> Google Webmaster
		                          </p>
		                        </a>
		                      </li>
		                      <li>
		                        <a href="http://www.charactercountonline.com/" target="_blank">
		                          <p>
		                            <i class="fa fa-link green"></i> Online Character Counter
		                          </p>
		                        </a>
		                      </li>

 													<hr>
		                      
		                      <li>
		                        <a href="https://api.topimobiliare.md/" target="_blank">
		                          <p>
		                            <i class="fa fa-link green"></i> API Top Imobiliare (Proiecte noi, cariera, oferte)
		                          </p>
		                        </a>
		                      </li>
		                      <li>
		                        <a href="https://papi.topimobiliare.md/" target="_blank">
		                          <p>
		                            <i class="fa fa-link green"></i> P-API Top Imobiliare (Parteneri)
		                          </p>
		                        </a>
		                      </li>

		                      <li>
		                        <a href="https://topimobiliare.md/wp-admin/" target="_blank">
		                          <p>
		                            <i class="fa fa-link green"></i> Panou de control (topimobiliare.md)
		                          </p>
		                        </a>
		                      </li>
		                    </ul>
		                  </div>
                      <!-- end of links -->

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
											@if($user->detalii !== null)
	                      <div class="profile_title">
	                        <div class="col-md-6">
	                          <h2>Detalii</h2>
	                        </div>
	                      </div>
	                      <div class="row">
	                      	<div class="col-xs-12">
	                      		{!! $user->detalii !!}
	                      	</div>
	                      </div>
	                    @endif
                      <!-- start of user-activity-graph -->
                      
                      <!-- end of user-activity-graph -->

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Task-uri active</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Task-uri finalizate</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
														@php
											      	$active = App\Seo::where('status', 0)->get();
											      @endphp
                            <!-- start recent activity -->
                            <ul class="messages">
                            	@foreach($active as $a)
                            	@php
                            		$usr = App\User::find($a->created_by);
											      		$dt = new Carbon\Carbon($a->created_at);
                            	@endphp
	                              <li>
	                                <img src="{{$usr->avatar}}" class="avatar" alt="Avatar">
	                                <div class="message_date">
	                                  <h3 class="date text-info">{{$dt->formatLocalized('%d')}}</h3>
	                                  <p class="month">{{$dt->formatLocalized('%B')}}</p>
	                                </div>
	                                <div class="message_wrapper">
	                                  <h4 class="heading">{{$user->name}}</h4>
	                                  <blockquote class="message">{{$a->titlu}}</blockquote>
	                                  <br>
	                                  <p class="url">
	                                    <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
	                                    <a href="/new/seo-task/{{$a->id}}"><i class="fa fa-link"></i> Detalii </a>
	                                  </p>
	                                </div>
	                              </li>
                              @endforeach

                            </ul>
                            <!-- end active tasks -->

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
														
                            @php
											      	$inactive = App\Seo::where('status', 1)->get();
											      @endphp
                            <!-- start recent activity -->
                            <ul class="messages">
                            	@forelse($inactive as $a)
                            	@php
                            		$usr = App\User::find($a->created_by);
											      		$dt = new Carbon\Carbon($a->created_at);
                            	@endphp
	                              <li>
	                                <img src="{{$usr->avatar}}" class="avatar" alt="Avatar">
	                                <div class="message_date">
	                                  <h3 class="date text-info">{{$dt->formatLocalized('%d')}}</h3>
	                                  <p class="month">{{$dt->formatLocalized('%B')}}</p>
	                                </div>
	                                <div class="message_wrapper">
	                                  <h4 class="heading">{{$user->name}}</h4>
	                                  <blockquote class="message">{{$a->titlu}}</blockquote>
	                                  <br>
	                                  <p class="url">
	                                    <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
	                                    <a href="/new/seo-task/{{$a->id}}"><i class="fa fa-link"></i> Detalii </a>
	                                  </p>
	                                </div>
	                              </li>
	                            @empty
	                            	<li>
	                                <div class="message_wrapper">
	                                  <blockquote class="message">Nu sunt task-uri finisate</blockquote>
	                                </div>
	                              </li>
                              @endforelse

                            </ul>
                            <!-- end finished tasks -->

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                              photo booth letterpress, commodo enim craft beer mlkshk </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
	</div>

 
</div>
<!-- /page content -->
@endsection

@section('custom-js')

@endsection