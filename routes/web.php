<?php

Route::group(['prefix' => 'new', 'middleware' => ['web', 'auth']], function () {

    Route::get("/", 'NicolaeCasir\Agenti\Http\HomeController@welcome');
    Route::get("/anunturi", 'NicolaeCasir\Agenti\Http\HomeController@gasitiAnunturi');
    Route::get("/anunturic", 'NicolaeCasir\Agenti\Http\HomeController@gasitiAnunturiComerciale');
    Route::get('/toti-furnizorii', 'NicolaeCasir\Agenti\Http\HomeController@totiFurnizorii');
    Route::get('/baza-clienti', 'NicolaeCasir\Agenti\Http\HomeController@bazaClienti');
    Route::get('/rapoarte', 'NicolaeCasir\Agenti\Http\HomeController@rapoarte');
    Route::get('/modificari-agenti', 'NicolaeCasir\Agenti\Http\HomeController@modificariAgenti');
    Route::get('/modificari-content', 'NicolaeCasir\Agenti\Http\HomeController@modificariContent');
    Route::get('/seo', 'NicolaeCasir\Agenti\Http\HomeController@seo');
    Route::get('/mesaje/{source}', 'NicolaeCasir\Agenti\Http\HomeController@mesaje');
    Route::get('/mesaje', 'NicolaeCasir\Agenti\Http\HomeController@redirectHome');
    Route::get('/vezi-mesaj/{source}/{id}', 'NicolaeCasir\Agenti\Http\HomeController@veziMesaj');
    Route::get('/mesaj-citit/{source}/{id}', 'NicolaeCasir\Agenti\Http\HomeController@marcheazaCaCitit');
    Route::get('/chat', 'NicolaeCasir\Agenti\Http\HomeController@chat');
    Route::get('/proiecte', 'NicolaeCasir\Agenti\Http\HomeController@proiecte');
    Route::get('/proiecte/companii', 'NicolaeCasir\Agenti\Http\HomeController@companii');

    Route::post('/add-task-seo', 'NicolaeCasir\Agenti\Http\HomeController@addTaskSeo');
    Route::get('/seo-task/{id}', 'NicolaeCasir\Agenti\Http\HomeController@seoTask');
    Route::post('/done-seo-task', 'NicolaeCasir\Agenti\Http\HomeController@doneSeoTask');
});

Route::group(['prefix' => 'new', 'middleware' => 'web'], function () {
    Route::get('/logout', 'NicolaeCasir\Agenti\Http\HomeController@logout');
    Route::get('/login', 'NicolaeCasir\Agenti\Http\HomeController@login')->name('login');
});

