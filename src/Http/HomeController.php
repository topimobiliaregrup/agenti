<?php

namespace NicolaeCasir\Agenti\Http;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\SeoUser;
use App\Seo;

class HomeController {

	public function welcome () {
		return view('agenti::welcome');
	}
	public function gasitiAnunturi () {
		return view('agenti::anunturi');
	}
	public function gasitiAnunturiComerciale () {
		return view('agenti::anunturic');
	}
	public function totiFurnizorii () {
		return view('agenti::toti-furnizorii');
	}
	public function bazaClienti () {
		return view('agenti::baza-clienti');
	}
	public function rapoarte () {
		return view('agenti::rapoarte');
	}
	public function modificariAgenti () {
		return view('agenti::modificari-agenti');
	}
	public function modificariContent () {
		return view('agenti::modificari-content');
	}
	public function mesaje ($source) {
		return view('agenti::mesaje')->with('source', $source);
	}
	public function redirectHome () {
		return redirect()->intended('/new');
	}
	public function veziMesaj ($source, $id){
		switch($source){
			case 'proiecte':
				$mesaj = DB::connection('api')->table('contacts')->where('id', $id)->first();
				if($mesaj === null) return redirect()->intended('/new');
				return view('agenti::mesaj')->with(['mesaj' => $mesaj, 'source' => 'proiecte']);
				break;

			case 'parteneri':
				$mesaj = DB::connection('parteneri')->table('messages')->where('id', $id)->first();
				if($mesaj === null) return redirect()->intended('/new');
				return view('agenti::mesaj')->with(['mesaj' => $mesaj, 'source' => 'parteneri']);
				break;

			case 'cparteneri':
				$mesaj = DB::connection('parteneri')->table('requests')->where('id', $id)->first();
				if($mesaj === null) return redirect()->intended('/new');
				return view('agenti::mesaj')->with(['mesaj' => $mesaj, 'source' => 'cparteneri']);
				break;
			default:
				return redirect()->intended('/new');
		}
	}
	public function marcheazaCaCitit ($source, $id) {
		switch($source){
			case 'proiecte':
				DB::connection('api')->table('contacts')->where('id', $id)->update([
					"read" => 1
				]);
				return redirect()->back();
				break;

			case 'parteneri':
				DB::connection('parteneri')->table('messages')->where('id', $id)->update([
					"read" => 1
				]);
				return redirect()->back();
				break;

			case 'cparteneri':
				$mesaj = DB::connection('parteneri')->table('requests')->where('id', $id)->update([
					"read" => 1
				]);
				return redirect()->back();
				break;
			default:
				return redirect()->intended('/new');
		}
	}
	public function chat () {
		return view('agenti::chat');
	}
	public function logout () {
		Auth::logout();
		return redirect()->intended('/new');
	}
	public function login () {
		return view('agenti::login');
	}
	public function proiecte () {
		return view('agenti::proiecte');
	}
	public function companii () {
		return view('agenti::companii');
	}
	public function seo() {
		if(Auth::User()->admin == 5) {
				$user = SeoUser::where('user_id', Auth::User()->id)->first();
				if($user !== null)
					return view('agenti::seo.index')->with('user', $user);
				else 
					return redirect()->intended('/new');
		}
		else return view('agenti::seo');
	}

	public function addTaskSeo (Request $r){
		SEO::create($r->toArray());
		return redirect()->back();
	}
	public function seoTask ($id) {
		$task = Seo::findOrFail($id);
		return view('agenti::seo.view-task')->with('task', $task);
	}
	public function doneSeoTask (Request $r) {
		Seo::where('id', $r->id)->update([
			"status" => 1,
			"executed_by" => Auth::User()->id,
			"comment" => $r->comment
		]);

		return redirect()->back();
	}

}