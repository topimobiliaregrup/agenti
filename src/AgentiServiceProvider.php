<?php

namespace NicolaeCasir\Agenti;

use Illuminate\Support\ServiceProvider;
use \Carbon\Carbon;
use Auth;

class AgentiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/../routes/web.php';
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'agenti');
        Carbon::setLocale('ro');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([__DIR__ . '/../publishable/assets' => public_path('assets')], 'assets');
    }
}
