let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'publishable/assets/')
		.sass('resources/assets/sass/style.scss', 'publishable/assets/');